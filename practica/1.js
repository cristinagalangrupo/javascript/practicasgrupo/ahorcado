var palabras = ["caracter","hola","agua","javascript","coche"]; // debe contener las palabras en MINÚSCULAS
var longitud;
var posicion;
var lIntroducidas=[]; // array que contiene las letras introducidas;
var acertaste=0; //almacena el número de letras que acierta para saber cuando ha acertado la palabra completa

/* *********************************** ALEATORIO *********************************** */


/* Devuelve un número entre 0 y la longitud menos 1 del array de palabras que es la posición de la palabra aleatoria*/
function aleatorio(arg){ // arg es el array que contiene las palabras (palabras)
	var x=0; // almacena la posición de la palabra aleatoria
	var l=0; // longitud del array
	l=arg.length;
	x=parseInt(Math.random()*l);;
	return x;

}

/* *********************************** CARGAR *********************************** */

function cargar() {
	//lIntroducidas=[];
	posicion=aleatorio(palabras); //posición de la palabra elegida al azar

    longitud = palabras[posicion].length; //Longitud de la palabra
    
/* *********************************** DIBUJAR CAJAS *********************************** */ 

 /* Dibujamos tantas cajas como letras tenga la palabra*/
    function dibujar(arg){
    var v = []; //array de div

    v = document.querySelector(".fila1")
    for (var i = 0; i < longitud; i++) {
        v.innerHTML += "<div class='letras'></div>";

    	}
	}
	dibujar(longitud);
}

/* *********************************** LEER CARACTER INTRODUCIDO *********************************** */ 

/* Creamos la función que lee los caracteres */
var c="";
function leer(){
	
	c=(document.querySelector("#letra").value).toLowerCase(); // leo el valor. Lo paso a minúsculas
	document.querySelector("#letra").value=""; //limpio el input
	comprobar(c); //compruebo la letra	
}

/* *********************************** ¿ESTÁ INTRODUCIDO? *********************************** */ 

/* Compruebo si la letra está introducida. Si está devuelve 1. */
function estaIntroducida(arg){ // le paso el caracter y comprueba si está introducida
	for(var i=0;i<lIntroducidas.length;i++){
		if (arg==lIntroducidas[i]) {
			return 1;
		}
	}

}

/* *********************************** ¿ESTÁ CONTENIDO EN LA PALABRA? *********************************** */ 

var posiciones=[];

function comprobar(arg){//Le pasamos la letra que introduce el usuario
	var si=estaIntroducida(c); // si devuelve 1 está introducida
	var str=""; //contiene la palabra con la que jugamos
	var contador=0; //Almacena cuántas veces NO coincide la letra en el string
	
	str=palabras[posicion]; 

	
	// Compruebo si está el carácter en la palabra y guardo las posiciones
	
	/* Letra ya introducida */

	if (si==1) {
		alert("La letra ya está introducida.");
	} else{

	/*Letra nueva */

	for(var i=0;i<longitud;i++){ //Almacena en el array posiciones, las posiciones donde coincida el carácter
		if(arg==str[i]){ //coincide la letra
			posiciones.push(i);
			acertaste++;

		}else{ //no coincide la letra
			contador++;
		}

	}
	if (contador==longitud) {
		escribirFallos(arg); //si la letra no está contenida en la palabra la escribe en fallos
		
	}else{
		escribirAciertos(arg,posiciones);
	}
	lIntroducidas.push(arg);

		if (acertaste==longitud) {
		alert("¡Acertaste la palabra!");
		// reseteo
		document.querySelector(".fila1").innerHTML="";
		document.querySelector(".fila2").innerHTML="";
		lIntroducidas=[];
		contador=0;
		acertaste=0;
		f=0;
		cargar();
	}
	}
}



var acumulador=0; // Almacena el número de coincidencias
var contador=0;

/* *********************************** ESCRIBIR ACIERTOS *********************************** */ 

/* Función para escribir los caracteres que acierta el usuario */
function escribirAciertos(arg1,arg2){ // arg1 es la letra que coincide, arg2 es el array con las posiciones de la letra de arg1
	var a=0;//almacena la posición del array de posiciones
	
	for(var i=0;i<arg2.length;i++){
		var a=arg2[i];
		var b=document.querySelectorAll(".letras")

		b[a].innerHTML+=arg1.toUpperCase();
		b[a].style.backgroundColor="#2ECC71";
	}

	contador=arg2.length;
	posiciones=[];//limpiar el array

	acumulador+=contador;
	if(acumulador==longitud){
	console.log("fin");
}
}

/* *********************************** ESCRIBIR FALLOS *********************************** */ 
/* Función para escribir los caracteres que falla el usuario */
var f=0; //posiciones de las cajas de los fallos
function escribirFallos(arg){
 v = document.querySelector(".fila2");
 v.innerHTML += "<div class='letrasF'></div>";
document.querySelectorAll(".letrasF")[f].innerHTML+=arg.toUpperCase();
f++;

}





